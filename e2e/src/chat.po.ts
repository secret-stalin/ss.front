import {$$, browser, by, element} from 'protractor';

export class ChatPage {

  private message = 'testingagain';


  navigateTo() {
    return browser.get('/chat/1');
  }

  sendmessage() {
    const input = element(by.id('input'));
    const send = element(by.id('send'));
    input.sendKeys(this.message);
    send.click();
  }
}
