import {browser, by, element} from 'protractor';
import {protractor} from 'protractor/built/ptor';

export class LoginPage {
  private credentials = {
    username: 'test@mail.com',
    password: 'jwtpass'
  };

  private wrongCredentials = {
    username: 'test@mail.com',
    password: 'wrongjwtpass'
  };

  navigateTo() {
    return browser.get('/login');
  }

  fillCredentials(credentials: any = this.credentials) {
    const button = element(by.id('loginButton'));
    element(by.name('email')).sendKeys(credentials.username);
    element(by.name('password')).sendKeys(credentials.password);
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(button), 10000).then(function () {
      button.click();
    });
  }

  fillIncorrectCredentials(credentials: any = this.wrongCredentials) {
    const button = element(by.id('loginButton'));
    element(by.name('email')).sendKeys(credentials.username);
    element(by.name('password')).sendKeys(credentials.password);
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(button), 10000).then(function () {
      button.click();
    });
  }

  checkErrorMessage() {
    return element(by.id('login-error-message')).getText();
  }
}
