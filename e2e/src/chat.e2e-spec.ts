import {LoginPage} from './login.po';
import {browser, by, element} from 'protractor';
import {MenuPage} from './menu.po';
import {TOKEN_NAME} from '../../src/app/services/auth.constant';
import {ChatPage} from './chat.po';

describe('workspace-project App', () => {
  let loginPage: LoginPage;
  let chatPage: ChatPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    chatPage = new ChatPage();
  });

  it('should send and receive chat', () => {
    loginPage.navigateTo();
    loginPage.fillCredentials();
    browser.waitForAngular();
    chatPage.navigateTo();
    browser.waitForAngular();
    chatPage.sendmessage();
    browser.sleep(1000);
    element.all(by.css('.message')).then(function (elements) {
      expect(elements[elements.length - 1].getText()).toContain('testingagain');
    });
  });
});
