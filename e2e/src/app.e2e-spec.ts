import {AppPage} from './app.po';
import {browser} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should open a web browser', () => {
    page.navigateTo();
    expect(browser.driver.getCurrentUrl()).toMatch('/');
  });
});
