import {browser, by, element} from 'protractor';
import {protractor} from 'protractor/built/ptor';

export class RegisterPage {
  private credentials = {
    email: 'testRegister@mail.com',
    password: 'test',
    username: 'testRegister'
  };

  navigateTo() {
    return browser.get('/register');
  }

  fillCredentials(credentias: any = this.credentials) {
    const button = element(by.id('registerButton'));
    element(by.name('email')).sendKeys(Math.floor((Math.random() * 10000000) + 1) + credentias.email);
    element(by.name('password')).sendKeys(credentias.password);
    element(by.name('username')).sendKeys(Math.floor((Math.random() * 10000000) + 1) + credentias.username);
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(button), 10000).then(function () {
      button.click();
    });

  }
}
