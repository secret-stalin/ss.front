import {RegisterPage} from './register.po';
import {browser} from 'protractor';

describe('workspace-project App', () => {
  let page: RegisterPage;
  beforeEach(() => {
    page = new RegisterPage();
  });

  it('should register', () => {
    page.navigateTo();
    page.fillCredentials();
    browser.waitForAngular();
    expect(browser.driver.getCurrentUrl()).toMatch('/register/confirm');
  });
});
