import {LoginPage} from './login.po';
import {browser} from 'protractor';
import {TOKEN_NAME} from '../../src/app/services/auth.constant';

describe('workspace-project App', () => {
  let page: LoginPage;
  beforeEach(() => {
    page = new LoginPage();
  });

  it('should login', () => {
    page.navigateTo();
    page.fillCredentials();
    browser.waitForAngular();
    expect(browser.driver.getCurrentUrl()).toMatch('/');
    const value = browser.executeScript('return window.localStorage.getItem("' + TOKEN_NAME + '");');
    expect(value).not.toBeNull();
  });

  it('should not login', () => {
    page.navigateTo();
    page.fillIncorrectCredentials();
    browser.waitForAngular();
    expect(page.checkErrorMessage().then(text => text.length > 0));
    expect(browser.driver.getCurrentUrl()).toMatch('/login');
  });
});
