import {LoginPage} from './login.po';
import {browser} from 'protractor';
import {MenuPage} from './menu.po';
import {TOKEN_NAME} from '../../src/app/services/auth.constant';

describe('workspace-project App', () => {
  let loginPage: LoginPage;
  let menuPage: MenuPage;

  beforeEach(() => {
    loginPage = new LoginPage();
    menuPage = new MenuPage();
  });

  it('should logout', () => {
    loginPage.navigateTo();
    loginPage.fillCredentials();
    browser.waitForAngular();
    menuPage.getTitle();
    expect(menuPage.titleElementText).toBe('Secret Stalin');
    menuPage.logout();
    browser.waitForAngular();
    const value = browser.executeScript('return window.localStorage.getItem("' + TOKEN_NAME + '");');
    expect(value).toBeNull();
  });
});
