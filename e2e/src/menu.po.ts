import {browser, by, element} from 'protractor';
import {protractor} from 'protractor/built/ptor';
import {log} from 'util';

export class MenuPage {

  private _titleElementText;

  logout() {
    const logoutBtn = element(by.id('logout'));
    logoutBtn.click();
  }

  getTitle() {
    this._titleElementText = element(by.css('#title')).getText();
  }

  get titleElementText() {
    return this._titleElementText;
  }

  set titleElementText(value) {
    this._titleElementText = value;
  }
}
