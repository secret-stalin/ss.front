import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TOKEN_NAME} from '../services/auth.constant';
import 'rxjs/add/operator/do';


const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem(TOKEN_NAME);
    let authReq = req;
    if (token != null) {
      authReq = req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)});
    } else {
    }

    return next.handle(authReq).do(
      (err: any) => {
        if (err instanceof HttpErrorResponse) {

          if (err.status === 401) {
          }
        }
      }
    );

  }

}
