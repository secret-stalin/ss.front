import {Role} from './model/role';

export const Roles: Role[] = [
  {name: 'Mayor', check: false},
  {name: 'Hunter', check: false}
];

