import { TestBed } from '@angular/core/testing';

import { ReplayService } from './replay.service';
import {AppModule} from '../../app.module';

describe('ReplayService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: ReplayService = TestBed.get(ReplayService);
    expect(service).toBeTruthy();
  });
});
