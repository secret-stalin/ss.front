import { Injectable } from '@angular/core';
import {APILOC} from '../auth.constant';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Replay} from '../../model/game/replay';

@Injectable({
  providedIn: 'root'
})
export class ReplayService {
  urlReplay = APILOC + '/api/replay/';

  constructor(private http: HttpClient) {}

  getReplay(gameId: number): Observable<Replay> {
    return this.http.get(this.urlReplay + gameId).map(res => <Replay>res);
}


}
