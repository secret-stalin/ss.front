import { Injectable } from '@angular/core';
import {APILOC} from '../auth.constant';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NotificationMessage} from '../../model/notification/notificationMessage';
import {NotificationSettings} from '../../model/notificationSettings';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private getNotificationsUrl: string;
  private getNotificationCountUrl: string;
  private readNotificationurl: string;
  private settingsUrl: string;

  constructor(private http: HttpClient) {
    this.getNotificationsUrl = APILOC + '/api/notifications';
    this.getNotificationCountUrl = APILOC + '/api/notifications/count';
    this.readNotificationurl = APILOC + '/api/notification/';
    this.settingsUrl = APILOC + '/api/notification/settings/';
  }

  getNotifications(): Observable<NotificationMessage[]> {
    return this.http.get<NotificationMessage[]>(this.getNotificationsUrl, {});
  }

  getNotificationCount(): Observable<number> {
    return this.http.get<number>(this.getNotificationCountUrl, {});
  }

  readNotification(notificationId: number): Observable<Object> {
    return this.http.post(this.readNotificationurl + notificationId, {});
  }

  sendSettings(settings: NotificationSettings) {
    console.log(settings);
    return this.http.post(this.settingsUrl, settings).subscribe();
  }
  readSettings(): Observable<NotificationSettings> {
    return this.http.get<NotificationSettings>(this.settingsUrl);
  }
}
