import {TestBed} from '@angular/core/testing';

import {NotificationService} from './notification.service';
import {AppModule} from '../../app.module';

describe('NotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });
});
