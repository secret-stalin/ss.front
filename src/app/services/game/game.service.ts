import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Settings} from '../../model/settings';
import {Game} from '../../model/game/game';
import {APILOC} from '../auth.constant';
import {HttpClient} from '@angular/common/http';
import {GameStats} from '../../model/game/gameStats';
import {RoleCard} from '../../model/game/roleCard';
import {Participation} from '../../model/game/participation';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private gameUrl = APILOC + '/api/game/';
  private gamelistUrl = APILOC + '/api/games/active';
  private voteUrl = APILOC + '/api/vote';
  private voteResultUrl = APILOC + '/api/voteResults/';
  private gameResultUrl = APILOC + '/api/game/stats/';
  private rolesUrl = APILOC + '/api/game/my-roles/';
  private currentParticipationUrl = APILOC + '/api/game/participation/';


  constructor(private http: HttpClient) {
  }

  getGame(): Observable<Settings> {
    return this.http.get<Settings>(this.gameUrl);
  }

  getGameById(gameId: number): Observable<Game> {
    return this.http.get<Game>(this.gameUrl + gameId);
  }

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gamelistUrl);
  }

  vote(gameId: number, affectedPlayerId: number) {
    return this.http.post(this.voteUrl, {gameId: gameId, affectedPlayerId: affectedPlayerId});
  }

  getVoteResult(gameId: number) {
    return this.http.get(this.voteResultUrl + gameId);
  }

  getGameStats(gameId: number): Observable<GameStats> {
    return this.http.get<GameStats>(this.gameResultUrl + gameId );
  }

  getRolesByParticipation(gameId: number): Observable<RoleCard[]> {
    return this.http.get<RoleCard[]>(this.rolesUrl + gameId);
  }

  getCurrentParticipation(gameId: number): Observable<Participation> {
    return this.http.get<Participation>(this.currentParticipationUrl + gameId);
  }

  doSpecialMove(gameId: number, selectedRoleAction: string, affectedPlayerId: number): Observable<any> {
    return this.http.post(this.voteUrl, {gameId: gameId, affectedPlayerId: affectedPlayerId, roleTypeExecutor: selectedRoleAction});
  }

  getTarget(gameId: number, selectedRoleAction: string): Observable<number> {
    return this.http.get<number>(this.gameUrl + gameId + '/target/' + selectedRoleAction);
  }
}
