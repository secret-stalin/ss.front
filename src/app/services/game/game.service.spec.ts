import { TestBed } from '@angular/core/testing';

import { GameService } from './game.service';
import {AppModule} from '../../app.module';

describe('GameService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: GameService = TestBed.get(GameService);
    expect(service).toBeTruthy();
  });
});
