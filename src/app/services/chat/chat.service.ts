import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Chat} from '../../model/chat/chat';
import {Message} from '../../model/chat/message';
import {APILOC} from '../auth.constant';
import {GameChat} from '../../model/chat/gameChat';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private getChatsUrl: string;
  private getMessagesUrl: string;
  private getGameChatUrl: string;

  constructor(private http: HttpClient) {
    this.getChatsUrl = APILOC + '/api/chat/getChats';
    this.getMessagesUrl = APILOC + '/api/chat/getMessages';
    this.getGameChatUrl = APILOC + '/api/game/chats';
  }

  getChatsFromUser(): Observable<Chat[]> {
    return this.http.get<Chat[]>(this.getChatsUrl, {});
  }

  getExistingMessages(chatId: string): Observable<Message[]> {
    return this.http.get<Message[]>(this.getMessagesUrl + '/' + chatId, {});
  }

  getGameChats(gameId: number): Observable<GameChat[]> {
    return this.http.get<GameChat[]>(this.getGameChatUrl + '/' + gameId, {});
  }
}
