import { TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';
import {AppModule} from '../../app.module';

describe('ChatService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: ChatService = TestBed.get(ChatService);
    expect(service).toBeTruthy();
  });
});
