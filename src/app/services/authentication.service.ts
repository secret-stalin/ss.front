import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

import {APILOC, TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME} from './auth.constant';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  static AUTH_TOKEN = APILOC + '/oauth/token';

  constructor(private http: Http) {

  }

  login(email: string, password: string) {
    const body = `username=${encodeURIComponent(email)}&password=${encodeURIComponent(password)}&grant_type=password`;

    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));

    return this.http.post(AuthenticationService.AUTH_TOKEN, body, {headers})
      .pipe(map(res => res.json()))
      .pipe(map((res: any) => {
          if (res.access_token) {
            return res.access_token;
          }
          return null;
        })
      );
  }
}
