import {Injectable} from '@angular/core';
import {JwtHelper} from 'angular2-jwt';
import {tokenNotExpired} from 'angular2-jwt';
import {APILOC, TOKEN_NAME} from '../auth.constant';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {User} from '../../model/user';
import {UserRegister} from '../../model/register/userRegister';
import {Userstats} from '../../model/userstats';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  jwtHelper: JwtHelper = new JwtHelper();
  accessToken: string;
  isAdmin: boolean;
  private userUrl = APILOC + '/api/user/';
  private getByUsernameUrl = APILOC + '/api/user/profile/';
  private allUsersUrl = APILOC + '/api/users';
  private isFriendFromUrl = APILOC + '/api/user/is-friend/';
  private hasRequestFromUrl = APILOC + '/api/user/has-friend-request/';
  private hasFriendRequestPendingToUserUrl = APILOC + '/api/user/has-friend-request-pending/';
  private sendFriendRequestUrl = APILOC + '/api/user/friend-request/send/';
  private acceptFriendRequestUrl = APILOC + '/api/user/friend-request/accept/';
  private declineFriendRequestUrl = APILOC + '/api/user/friend-request/decline/';
  private removeAsFriendUrl = APILOC + '/api/user/remove-friend/';
  private getUserByIdUrl = APILOC + '/api/user/id/';
  private getUserStatsUrl = APILOC + '/api/user/stats/';
  subject: Subject<boolean>;

  constructor(private http: HttpClient) {
    this.subject = new Subject();
    if (this.isLoggedIn()) {
      this.subject.next(true);
    } else {
      this.subject.next(false);
    }
  }

  login(accessToken: string) {
    this.accessToken = accessToken;
    localStorage.setItem(TOKEN_NAME, accessToken);
    this.subject.next(true);
  }

  logout() {
    this.accessToken = null;
    this.isAdmin = false;
    localStorage.removeItem(TOKEN_NAME);
    this.subject.next(false);
  }

  isLoggedIn(): boolean {
    return tokenNotExpired(TOKEN_NAME, this.accessToken);
  }


  findFriends(userName: string): Observable<User[]> {
    return this.http.get(this.userUrl + userName).map(res => <User[]>res);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get(this.allUsersUrl).map(res => <User[]>res);
  }

  getEmail(): string {
    this.accessToken = localStorage.getItem(TOKEN_NAME);
    const decodedToken = this.jwtHelper.decodeToken(this.accessToken);
    return decodedToken.user_name;
  }

  getByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.getByUsernameUrl + username);
  }

  getSubject(): Subject<boolean> {
    return this.subject;
  }

  isFriendFrom(username: string): Observable<boolean> {
    return this.http.get<boolean>(this.isFriendFromUrl + username);
  }

  hasFriendRequestFrom(username: string): Observable<boolean> {
    return this.http.get<boolean>(this.hasRequestFromUrl + username);
  }

  hasFriendRequestPendingToUser(username: string): Observable<boolean> {
    return this.http.get<boolean>(this.hasFriendRequestPendingToUserUrl + username);
  }

  sendFriendRequest(userName: string): Observable<Object> {
    return this.http.post(this.sendFriendRequestUrl, userName);
  }

  acceptFriendRequest(userName: string): Observable<Object> {
    return this.http.post(this.acceptFriendRequestUrl, userName);
  }

  declineFriendRequest(userName: string): Observable<Object> {
    return this.http.post(this.declineFriendRequestUrl, userName);
  }

  getUserNameById(targetId: number): Observable<UserRegister> {
    return this.http.get<UserRegister>(this.getUserByIdUrl + targetId);
  }

  removeAsFriend(userName: string): Observable<Object> {
    return this.http.post(this.removeAsFriendUrl, userName);
  }

  getUser(): Observable<User> {
    return this.http.get<User>(this.userUrl);
  }

  getUserStats(username: string): Observable<Userstats> {
    return this.http.get<Userstats>(this.getUserStatsUrl + username);
  }

  saveUser(user: User): Observable<Object> {
    return this.http.post(this.userUrl + 'change/', user);
  }
}
