import {TestBed} from '@angular/core/testing';

import {RegisterService} from './register.service';
import {HttpClientModule} from '@angular/common/http';
import {AppModule} from '../../app.module';

describe('RegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  it('should be created', () => {
    const service: RegisterService = TestBed.get(RegisterService);
    expect(service).toBeTruthy();
  });
});
