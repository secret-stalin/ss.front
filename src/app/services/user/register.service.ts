import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpClientModule, HttpResponse} from '@angular/common/http';
import {UserRegister} from '../../model/register/userRegister';
import {Observable} from 'rxjs';
import {APILOC} from '../auth.constant';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  urlRegistration = APILOC + '/api/registration';
  urlResend = APILOC + '/api/registration/resend';

  constructor(private http: HttpClient) {
  }

  register(userDTO: UserRegister): Observable<Object> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.urlRegistration, userDTO, httpOptions);
  }

  resendVerificationEmail(email: string): Observable<Object> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.urlResend, email, httpOptions);
  }
}
