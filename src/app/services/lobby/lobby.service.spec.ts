import {TestBed} from '@angular/core/testing';

import {LobbyService} from './lobby.service';
import {AppModule} from '../../app.module';

describe('LobbyService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: LobbyService = TestBed.get(LobbyService);
    expect(service).toBeTruthy();
  });
});
