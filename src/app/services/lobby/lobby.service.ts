import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../../model/user';
import {APILOC} from '../auth.constant';
import {HttpClient} from '@angular/common/http';
import {Settings} from '../../model/settings';
import {Lobby} from '../../model/lobby';
import {Role} from '../../model/role';
import {Roles} from '../../mock-roles';
import {Game} from '../../model/game/game';


@Injectable({
  providedIn: 'root'
})
export class LobbyService {
  private lobbyUrl = APILOC + '/api/lobby/';
  private inviteUrl = APILOC + '/api/lobby/';
  private lobbyStartUrl = APILOC + '/api/lobby/start/';
  private lobbyPublicUrl = APILOC + '/api/lobby/public/';
  private acceptInvitationUrl = APILOC + '/api/accepted/';
  private activeLobbyUrl = APILOC + '/api/lobby/active';

  constructor(private http: HttpClient) {
  }

  getJoinedUsers(lobbyId: number): Observable<User[]> {
    return this.http.get(this.lobbyUrl + 'joined/' + lobbyId).map(res => <User[]>res);
  }

  invite(lobbyId: number, userName: string) {
    this.http.post(this.inviteUrl + lobbyId + '/invite/' + userName, {}).subscribe();
  }

  makeGame(setting: Settings): Observable<Lobby> {
    return this.http.post<Lobby>(this.lobbyUrl + setting.gameId, setting);
  }

  startGame(lobbyId: number): Observable<Game> {
    return this.http.post<Game>(this.lobbyStartUrl +  lobbyId, {});
  }

  getPublicLobbys(): Observable<Lobby[]> {
    return this.http.get(this.lobbyPublicUrl).map(res => <Lobby[]>res);
  }

  joinLobby(lobbyId: number) {
    return this.http.post(this.lobbyPublicUrl + lobbyId, {});
  }

  acceptinvitation(invitationId: number): Observable<number> {
    return this.http.get<number>(this.acceptInvitationUrl + invitationId, {});
  }

  getActiveLobbies(): Observable<Lobby[]> {
    return this.http.get(this.activeLobbyUrl).map(res => <Lobby[]>res);
  }
}
