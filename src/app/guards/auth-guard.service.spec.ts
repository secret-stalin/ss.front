import {getTestBed, TestBed} from '@angular/core/testing';

import {AuthGuard} from './auth-guard.service';
import {AppModule} from '../app.module';

describe('AuthGuardService', () => {
  // tslint:disable-next-line:max-line-length
  beforeEach(() => TestBed.configureTestingModule({
     imports: [AppModule]
   }));

  it('should be created', () => {
    const service: AuthGuard = TestBed.get(AuthGuard);
    expect(service).toBeTruthy();
  });
});
