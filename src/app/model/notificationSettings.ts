export class NotificationSettings {
 public ignoreFriendRequest: boolean;
 public ignoreInvite: boolean;
 public ignoreEndPhase: boolean;
 public ignoreYourTurn: boolean;

  constructor() {
    this.ignoreFriendRequest = false;
    this.ignoreInvite = false;
    this.ignoreEndPhase = false;
    this.ignoreYourTurn = false;
  }


}
