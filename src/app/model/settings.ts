import {Role} from './role';
import {Roles} from '../mock-roles';

export class Settings {
  _gameId: number;
  _gameName: string;
  _maxPlayers: number;
  _isPrivate: boolean;
  _dayTime: string;
  _nightTime: string;
  _roles: string[];


  get gameId(): number {
    return this._gameId;
  }


  public get gameName(): string {
    return this._gameName;
  }

  get maxPlayers(): number {
    return this._maxPlayers;
  }

  get isPrivate(): boolean {
    return this._isPrivate;
  }

  get dayTime(): string {
    return this._dayTime;
  }

  get nightTime(): string {
    return this._nightTime;
  }

  get roles(): string[] {
    return this._roles;
  }

  getRole(name: string): Role {
    return Roles.find(r => r.name === name);
  }

  set gameId(value: number) {
    this._gameId = value;
  }
  public set gameName(value: string) {
    this._gameName = value;
  }

  set maxPlayers(value: number) {
    this._maxPlayers = value;
  }

  set isPrivate(value: boolean) {
    this._isPrivate = value;
  }

  set dayTime(value: string) {
    this._dayTime = value;
  }

  set nightTime(value: string) {
    this._nightTime = value;
  }

  set roles(value: string[]) {
    this._roles = value;
  }
}
