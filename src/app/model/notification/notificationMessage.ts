import {NotificationMessageType} from './notificationMessageType';

export class NotificationMessage {
  notificationId: number;
  targetId: number;
  text: string;
  read: boolean;
  notificatoinType: NotificationMessageType;
  userId: number;

  constructor() {
  }
}
