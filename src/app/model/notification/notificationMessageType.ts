export enum NotificationMessageType {
  FRIEND_REQUEST = 'FRIEND_REQUEST',
  INVITE = 'INVITE' ,
  ENDPHASE = 'ENDPHASE' ,
  YOUR_TURN = 'YOUR_TURN' ,
}
