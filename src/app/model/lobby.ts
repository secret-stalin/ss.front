import {User} from './user';
import {Settings} from './settings';

export class Lobby {
  lobbyId: number;
  users: User[];
  gameSettings: Settings;

}
