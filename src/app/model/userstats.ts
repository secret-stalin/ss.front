export class Userstats {
  gamesPlayedAsSlav: number;
  gameWonAsSlav: number;
  gamePlayedAsStalin: number;
  gamesWonAsStalin: number;

  constructor() {
  }
}
