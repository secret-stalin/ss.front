export class Chat {
  lastMessage: string;
  lastMessageTime: string;
  receiver: string;
  chatId: string;

  constructor() {
    this.lastMessage = '';
    this.lastMessageTime = '';
    this.receiver = '';
    this.chatId = '';
  }
}
