import {UserRegister} from '../register/userRegister';


export class Participation {
  participationId: number;
  alive: boolean;
  colorCode: string;
  userDTO: UserRegister;
  roleNames: string[];


  constructor() {
    this.participationId = 0;
    this.alive = true;
    this.colorCode = '';
    this.userDTO = new UserRegister();
    this.roleNames = [];
  }
}
