export class Action {
  gameId: number;
  win: boolean;
  endPhase: boolean;
  roleTypeWinner: String;
  affectedPlayerId: number;
  roleTypeExecutor: string;


  constructor() {
    this.gameId = 0;
    this.win = false;
    this.endPhase = false;
    this.roleTypeWinner = '';
    this.affectedPlayerId = 0;
    this.roleTypeExecutor = '';
  }
}
