import {Participation} from './participation';

export class Vote {
  public executor: Participation;
  public affected: Participation;


  constructor(executor: Participation, affected: Participation) {
    this.executor = executor;
    this.affected = affected;
  }
}
