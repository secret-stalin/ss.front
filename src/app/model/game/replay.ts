import {Participation} from './participation';
import {Event} from './event';

export class Replay {
  public events: Event[];
  public participations: Participation[];

  constructor() {
    this.events = [];
    this.participations = [];
  }
}
