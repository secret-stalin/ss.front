import {Message} from '../chat/message';

export class Event {
  public timestamp: string;
  public eventType: string;
  public chatAccess: string;
  public message: Message;
  public affectedPlayerId: number;
  public executorId: number;


  constructor(timestamp: string, eventType: string, chatAccess: string,
              message: Message, affectedPlayerId: number, executorId: number) {
    this.timestamp = timestamp;
    this.eventType = eventType;
    this.chatAccess = chatAccess;
    this.message = message;
    this.affectedPlayerId = affectedPlayerId;
    this.executorId = executorId;
  }
}
