export class Win {
  win: boolean;
  roleTypeWinner: string;

  constructor() {
    this.win = false;
    this.roleTypeWinner = '';
  }
}
