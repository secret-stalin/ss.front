export class UserRegister {
  email: string;
  username: string;
  newPassword: string;
  profilePicture: string;

  constructor() {
    this.email = '';
    this.username = '';
    this.newPassword = '';
    this.profilePicture = '';
  }
}
