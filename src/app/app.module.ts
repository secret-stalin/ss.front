import {BrowserModule} from '@angular/platform-browser';
import {NgModule , LOCALE_ID} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../material';

import {GameComponent} from './components/game/game.component';
import {ChatComponent} from './components/chat/chat.component';
import {MenuComponent} from './components/menu/menu.component';
import {GameChatComponent} from './components/game-chat/game-chat.component';
import {RegisterComponent} from './components/register/register.component';
import {AuthConfig, AuthHttp} from 'angular2-jwt';
import {Http, HttpModule} from '@angular/http';
import {AuthenticationService} from './services/authentication.service';
import {UserService} from './services/user/user.service';
import {AuthGuard} from './guards/auth-guard.service';
import {TOKEN_NAME} from './services/auth.constant';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './components/login/login.component';
import {EmailConfirmComponent} from './components/email-confirm/email-confirm.component';
import {ResendEmailComponent} from './components/resend-email/resend-email.component';
import {ErrorComponent} from './components/error/error.component';
import {LobbyComponent} from './components/lobby/lobby.component';
import {GameSettingsComponent} from './components/game-settings/game-settings.component';
import {ChatlistComponent} from './components/chatlist/chatlist.component';
import {Interceptor} from './interceptor/app.interceptor';
import {GameboardComponent} from './components/gameboard/gameboard.component';
import {PlayerComponent} from './components/player/player.component';
import {GamesComponent} from './components/games/games.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {LobbiesPublicComponent} from './components/lobbies-public/lobbies-public.component';
import {NotificationComponent} from './components/notification/notification.component';
import {HeaderComponent} from './components/header/header.component';
import {ProfileComponent} from './components/profile/profile.component';
import {UserEditComponent} from './components/user-edit/user-edit.component';
import {MatProgressBarModule} from '@angular/material';
import {GameReplayComponent} from './components/game-replay/game-replay.component';
import {LobbiesActiveComponent} from './components/lobbies-active/lobbies-active.component';

export function authHttpServiceFactory(http: Http) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: 'Bearer',
    tokenName: TOKEN_NAME,
    globalHeaders: [{'Content-Type': 'application/json'}],
    noJwtError: false,
    noTokenScheme: true,
    tokenGetter: (() => localStorage.getItem(TOKEN_NAME))
  }), http);
}

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    ChatComponent,
    LoginComponent,
    MenuComponent,
    GameChatComponent,
    RegisterComponent,
    EmailConfirmComponent,
    ResendEmailComponent,
    ErrorComponent,
    LobbyComponent,
    GameSettingsComponent,
    ErrorComponent,
    ChatlistComponent,
    GameboardComponent,
    PlayerComponent,
    GamesComponent,
    LobbiesPublicComponent,
    NotificationComponent,
    ProfileComponent,
    HeaderComponent,
    NotificationComponent,
    UserEditComponent,
    LobbiesActiveComponent,
    GameReplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    MatProgressBarModule
  ],
  providers: [
    {provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http]},
    AuthenticationService,
    UserService,
    AuthGuard,
    {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true},
    {provide: LOCALE_ID, useValue: 'en' }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
