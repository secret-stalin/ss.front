import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {MenuComponent} from './components/menu/menu.component';
import {RegisterComponent} from './components/register/register.component';
import {EmailConfirmComponent} from './components/email-confirm/email-confirm.component';
import {ResendEmailComponent} from './components/resend-email/resend-email.component';
import {ErrorComponent} from './components/error/error.component';
import {AuthGuard} from './guards/auth-guard.service';
import {GameComponent} from './components/game/game.component';
import {LobbyComponent} from './components/lobby/lobby.component';
import {GameSettingsComponent} from './components/game-settings/game-settings.component';
import {ChatlistComponent} from './components/chatlist/chatlist.component';
import {ChatComponent} from './components/chat/chat.component';
import {GameChatComponent} from './components/game-chat/game-chat.component';
import {GamesComponent} from './components/games/games.component';
import {LobbiesPublicComponent} from './components/lobbies-public/lobbies-public.component';
import {ProfileComponent} from './components/profile/profile.component';
import {UserEditComponent} from './components/user-edit/user-edit.component';
import {LobbiesActiveComponent} from './components/lobbies-active/lobbies-active.component';
import {GameReplayComponent} from './components/game-replay/game-replay.component';

const routes: Routes = [
  {path: 'menu', component: MenuComponent, canActivate: [AuthGuard]},
  {path: 'game/:id', component: GameComponent, canActivate: [AuthGuard]},
  {path: 'lobby/:id', component: LobbyComponent, canActivate: [AuthGuard]},
  {path: 'settings', component: GameSettingsComponent, canActivate: [AuthGuard]},
  {path: 'error', component: ErrorComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register/confirm', component: EmailConfirmComponent},
  {path: 'register/resend', component: ResendEmailComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'game/chat/:id', component: GameChatComponent, canActivate: [AuthGuard]},
  {path: 'games/active', component: GamesComponent, canActivate: [AuthGuard]},
  {path: 'chat/:id', component: ChatComponent, canActivate: [AuthGuard]},
  {path: 'chatlist', component: ChatlistComponent, canActivate: [AuthGuard]},
  {path: 'join', component: LobbiesPublicComponent, canActivate: [AuthGuard]},
  {path: 'user/profile/:username', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'user/edit', component: UserEditComponent, canActivate: [AuthGuard]},
  {path: 'activelobbies', component: LobbiesActiveComponent, canActivate: [AuthGuard]},
  {path: 'game/replay/:id', component: GameReplayComponent, canActivate: [AuthGuard]},
  {
    path: '**',
    component: MenuComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
