import {Component, OnInit} from '@angular/core';
import {Settings} from '../../model/settings';
import {Role} from '../../model/role';
import {Router} from '@angular/router';
import {LobbyService} from '../../services/lobby/lobby.service';
import {GameService} from '../../services/game/game.service';


@Component({
  selector: 'app-game-settings',
  templateUrl: './game-settings.component.html',
  styleUrls: ['./game-settings.component.scss']
})
export class GameSettingsComponent implements OnInit {
  loading = true;
  settings: Settings;
  roles: Role[];
  lobbyId: number;

  constructor(private router: Router, private lobbyService: LobbyService, private gameService: GameService) {
  }

  ngOnInit() {
    this.settings = new Settings();
    this.setDefaultSettingsIfNull();
    this.createGame();
    this.roles = [
      {name: 'Mayor', check: false},
      {name: 'Hunter', check: false}
    ];
  }

  setDefaultSettingsIfNull() {
    if (this.settings.gameName === '' || this.settings.gameName === undefined) {
      this.settings.gameName = 'Game' + Math.round(Math.random() * 9000 + 1000);
    }
    if (this.settings.roles === undefined) {
      this.settings.roles = [];
    }

    if (this.settings.maxPlayers === undefined) {
      this.settings.maxPlayers = 4;
    }
    if (this.settings.dayTime === undefined) {
      this.settings.dayTime = '10';
    }
    if (this.settings.nightTime === undefined) {
      this.settings.nightTime = '5';
    }
    if (this.settings.isPrivate === undefined) {
      this.settings.isPrivate = true;
    }
  }

  createGame() {
    // get game
    this.gameService.getGame().subscribe(gameJson => this.settings.gameId = gameJson.gameId);
    this.loading = false;

  }

  startLobby() {
    // post settings
    this.setDefaultSettingsIfNull();
    this.lobbyService.makeGame(this.settings).subscribe(res => {
      this.lobbyId = res.lobbyId;
      this.router.navigate(['/lobby/' + this.lobbyId]);
    });
  }
}
