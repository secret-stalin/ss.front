import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../../services/user/register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-resend-email',
  templateUrl: './resend-email.component.html',
  styleUrls: ['./resend-email.component.scss']
})
export class ResendEmailComponent implements OnInit {
  emailDTO = '';
  error = '';

  constructor(private router: Router, private registerService: RegisterService) { }

  ngOnInit() {
  }

  resendEmail() {
    this.registerService.resendVerificationEmail(this.emailDTO).subscribe(
      (val) => this.router.navigateByUrl('/register/confirm'),
      error => {
        this.error = error.error;
      }
    );
  }

}
