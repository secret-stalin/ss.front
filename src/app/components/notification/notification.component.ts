import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotificationMessage} from '../../model/notification/notificationMessage';
import {NotificationMessageType} from '../../model/notification/notificationMessageType';
import {Router} from '@angular/router';
import {NotificationService} from '../../services/notification/notification.service';
import {UserService} from '../../services/user/user.service';
import {LobbyService} from '../../services/lobby/lobby.service';
import {NotificationSettings} from '../../model/notificationSettings';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  @Input() notification: NotificationMessage = new NotificationMessage();
  @Output() childEvent = new EventEmitter();

  private settings: NotificationSettings;

  constructor(private router: Router, private notificationService: NotificationService,
              private userService: UserService, private lobbyService: LobbyService) {
  }

  ngOnInit() {
    this.notificationService.readSettings().subscribe(res => this.settings = <NotificationSettings>res);
  }

  navigate() {
    switch (this.notification.notificatoinType) {
      case NotificationMessageType.FRIEND_REQUEST : {
        this.notificationService.readNotification(this.notification.notificationId).subscribe(res => {
          this.userService.getUserNameById(this.notification.targetId).subscribe(message => {
            this.router.navigate(['/user/profile/' + message.username]);
          });
          this.childEvent.emit();
        });
        break;
      }
      case NotificationMessageType.INVITE : {
        this.notificationService.readNotification(this.notification.notificationId).subscribe(res => {
          this.childEvent.emit();
          this.lobbyService.acceptinvitation(this.notification.targetId).subscribe(lobbyId => {
            this.router.navigate(['/lobby/' + lobbyId]);
          });
        });
        break;
      }
      case NotificationMessageType.ENDPHASE : {
        this.notificationService.readNotification(this.notification.notificationId).subscribe(res => {
          this.router.navigate(['/game/' + this.notification.targetId]);
          this.childEvent.emit();
        });
        break;
      }
      case NotificationMessageType.YOUR_TURN : {
        this.notificationService.readNotification(this.notification.notificationId).subscribe(res => {
          this.router.navigate(['/game/' + this.notification.targetId]);
          this.childEvent.emit();
        });
        break;
      }
    }
  }

}
