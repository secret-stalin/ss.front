import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameReplayComponent } from './game-replay.component';
import {AppModule} from '../../app.module';

describe('GameReplayComponent', () => {
  let component: GameReplayComponent;
  let fixture: ComponentFixture<GameReplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameReplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
