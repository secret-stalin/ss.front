import {AfterViewChecked, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ReplayService} from '../../services/replay/replay.service';
import {ActivatedRoute} from '@angular/router';
import {Replay} from '../../model/game/replay';
import {Event} from '../../model/game/event';
import {Message} from '../../model/chat/message';
import {Game} from '../../model/game/game';
import {GameService} from '../../services/game/game.service';
import {Vote} from '../../model/game/vote';


@Component({
  selector: 'app-game-replay',
  templateUrl: './game-replay.component.html',
  styleUrls: ['./game-replay.component.scss']
})
export class GameReplayComponent implements OnInit, OnDestroy, AfterViewChecked {
  gameId: number;
  replay: Replay;
  counter: number;
  interval: any;
  game: Game;

  playersTop = [];
  playersLeft = [];
  playersRight = [];
  playersBottom = [];
  slavText = [];
  stalinText = [];
  voteText = [];
  voteResults: Vote[];
  maxVotes = 0;

  @ViewChild('scrollSlavMessageBox') private slavMessageBox: ElementRef;
  @ViewChild('scrollStalinMessageBox') private stalinMessageBox: ElementRef;
  @ViewChild('scrollVoteBox') private voteBox: ElementRef;

  constructor(private replayService: ReplayService, private route: ActivatedRoute, private gameService: GameService) {
    this.gameId = +this.route.snapshot.paramMap.get('id');
    this.gameService.getGameById(this.gameId).subscribe(game => this.game = game);
    this.replay = new Replay();
    this.voteResults = [];
    this.replayService.getReplay(this.gameId).subscribe(res => {
        for (const participation of res.participations) {
          participation.alive = true;
        }
        this.replay.participations = res.participations;
        this.replay.events = res.events;
      },
      function (error) {
      }, () => this.fillPlayers()
    );
  }

  ngOnInit() {
    this.counter = 0;
    this.interval = setInterval(() => {
      if (this.replay.events[this.counter] !== undefined) {
        this.handelEvents(this.replay.events[this.counter++]);
      } else {
        clearInterval(this.interval);
      }
    }, 1000);
    this.calculateVotes();
    this.scrollToBottom();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  private fillPlayers() {
    this.playersTop = [];
    this.playersLeft = [];
    this.playersRight = [];
    this.playersBottom = [];
    let i = 0;
    for (const player of this.replay.participations) {
      if (i === 0) {
        this.playersTop.push(player);
      } else if (i === 1) {
        this.playersBottom.push(player);
      } else if (i === 2) {
        this.playersRight.push(player);
      } else {
        this.playersLeft.push(player);
        i = -1;
      }
      i++;
    }
  }

  private handelEvents(event: Event) {
    if (event.eventType === 'CHAT') {
      this.addToChat(event.chatAccess, event.message);
    } else if (event.eventType === 'VOTE') {
      this.addToVotes(event.executorId, event.affectedPlayerId);
    } else if (event.eventType === 'ENDPHASE') {
      this.voteText.push('Phase has ended');
      this.voteResults = [];
    } else if (event.eventType === 'KILL') {
      this.addToKills(event.affectedPlayerId);
    }
  }

  private addToChat(role: string, message: Message) {
    if (role === 'SLAV') {
      this.slavText.push(message.text);
    } else {
      this.stalinText.push(message.text);
    }
  }

  private addToVotes(executorId: number, affectedPlayerId: number) {
    const executorPart = this.replay.participations.filter(p => p.participationId === executorId)[0];
    const affectedPart = this.replay.participations.filter(p => p.participationId === affectedPlayerId)[0];
    this.voteText.push(executorPart.userDTO.username + ' voted for ' + affectedPart.userDTO.username);
    if (this.voteResults.filter(v => v.executor.participationId === executorId).length === 0) {
      const vote = new Vote(executorPart, affectedPart);
      this.voteResults.push(vote);
    } else {
      this.voteResults.filter(v => v.executor.participationId === executorId)[0].affected = affectedPart;
    }
    this.calculateVotes();
  }

  private addToKills(affectedPlayerId: number) {
    const affectedPart = this.replay.participations.filter(p => p.participationId === affectedPlayerId)[0];
    this.voteText.push(affectedPart.userDTO.username + ' has been killed');
    affectedPart.alive = false;
  }

  private calculateVotes() {
    this.maxVotes = this.voteResults.length;
  }

  getVoteResult(vote: Vote): number {
    return this.voteResults.filter(v => v.affected.participationId === vote.affected.participationId).length;
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.slavMessageBox.nativeElement.scrollTop = this.slavMessageBox.nativeElement.scrollHeight;
      this.stalinMessageBox.nativeElement.scrollTop = this.stalinMessageBox.nativeElement.scrollHeight;
      this.voteBox.nativeElement.scrollTop = this.voteBox.nativeElement.scrollHeight;
    } catch (err) {}
  }
}
