import { Component, OnInit } from '@angular/core';
import {Game} from '../../model/game/game';
import {GameService} from '../../services/game/game.service';

@Component({
  selector: 'app-active-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {
  games: Game[];

  constructor(private service: GameService) {
    service.getGames().subscribe(res => {
      this.games = res;
    });
  }

  ngOnInit() {
  }

}
