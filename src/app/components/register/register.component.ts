import {Component, OnInit} from '@angular/core';
import {UserRegister} from '../../model/register/userRegister';
import {RegisterService} from '../../services/user/register.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: UserRegister = new UserRegister();
  error = '';
  loading = false;
  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private registerService: RegisterService) {
  }

  ngOnInit() {
  }

  register() {
    this.loading = true;
    this.registerService.register(this.user).subscribe(
      (val) => {
        this.loading = false;
        this.router.navigateByUrl('/register/confirm');
      },
      error => {
        this.error = error.error;
        this.loading = false;
      }
    );
  }
}
