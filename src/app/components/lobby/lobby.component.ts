import {Component, OnDestroy} from '@angular/core';
import {User} from '../../model/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {APILOC, TOKEN_NAME} from '../../services/auth.constant';
import * as StompJS from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {LobbyService} from '../../services/lobby/lobby.service';


@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnDestroy {
  users: User[];
  currentUser: User = new User();
  lobbyId: number;
  private stomp;
  private stompClient;
  private serverUrl = APILOC + '/socket?access_token=' + localStorage.getItem(TOKEN_NAME);

  constructor(private router: Router, private route: ActivatedRoute, private service: LobbyService, private userService: UserService) {
    this.lobbyId = +this.route.snapshot.paramMap.get('id');
    this.service.getJoinedUsers(this.lobbyId).subscribe(
      res => {
        this.users = res;
        this.findFriends(this.userService.getEmail());
        this.getCurrentUser();
        this.initializeWebSocketConnection();
      });
  }

  startGame() {
    this.service.startGame(this.lobbyId).subscribe(res => {
      this.router.navigate(['/game/' + res.gameId]);
    });
  }

  findFriends(mail: string) {
    const user = this.users.find(u => u.email === mail);
    this.userService.findFriends(mail).subscribe(res => user.friends = res);
  }

  getCurrentUser() {
    this.currentUser = this.users.find(u => u.email === this.userService.getEmail());
  }

  invite(userName: string) {
    this.service.invite(this.lobbyId, userName);
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stomp = StompJS.Stomp;
    this.stompClient = this.stomp.over(ws);
    this.stompClient.debug = () => {};
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/lobby/' + that.lobbyId, (message) => {
        if (message.body) {
          const incoming: User[] = JSON.parse(message.body);
            that.users = incoming;
        }
      });
      that.stompClient.subscribe('/lobby/start/' + that.lobbyId, (message) => {
        if (message.body) {
          const gameId: number = message.body ;
          that.router.navigate(['/game/' + gameId]);
        }
      });
    });
  }

  ngOnDestroy() {
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
  }
}
