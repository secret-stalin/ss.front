import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbiesActiveComponent } from './lobbies-active.component';
import {AppModule} from '../../app.module';

describe('LobbiesActiveComponent', () => {
  let component: LobbiesActiveComponent;
  let fixture: ComponentFixture<LobbiesActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbiesActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
