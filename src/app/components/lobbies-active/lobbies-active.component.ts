import {Component, OnInit} from '@angular/core';
import {Lobby} from '../../model/lobby';
import {LobbyService} from '../../services/lobby/lobby.service';

@Component({
  selector: 'app-active-lobbys',
  templateUrl: './lobbies-active.component.html',
  styleUrls: ['./lobbies-active.component.scss']
})
export class LobbiesActiveComponent implements OnInit {
  lobbies: Lobby[];

  constructor(private lobbyservice: LobbyService) {
  }

  ngOnInit() {
    this.getActiveLobbies();
  }

  getActiveLobbies() {
    this.lobbyservice.getActiveLobbies().subscribe(res => this.lobbies = res);
  }

}
