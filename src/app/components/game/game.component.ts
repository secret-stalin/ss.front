import {AfterViewChecked, Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GameService} from '../../services/game/game.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Game} from '../../model/game/game';
import {APILOC, TOKEN_NAME} from '../../services/auth.constant';
import * as StompJS from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {Action} from '../../model/game/action';
import {GameStats} from '../../model/game/gameStats';
import {RoleCard} from '../../model/game/roleCard';
import {Participation} from '../../model/game/participation';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy, AfterViewChecked {
  gameId: number;
  dto: Game;
  currentParticipation: Participation = new Participation();
  win: boolean;
  voteResults;
  gameStats: GameStats;
  liveStats: GameStats;
  roles: RoleCard[] = [];
  selectedRoleAction: string;
  target: number;
  participationIdVotedOn: number;

  private serverUrl = APILOC + '/socket?access_token=' + localStorage.getItem(TOKEN_NAME);
  private stompClient;
  private stomp;

  @ViewChild('scrollKillBox') private killBox: ElementRef;

  constructor(private router: Router, private gameService: GameService, private route: ActivatedRoute, private zone: NgZone) {
    this.roles = [];
    this.gameStats = new GameStats();
    this.liveStats = new GameStats();
    this.win = false;
    this.gameId = +this.route.snapshot.paramMap.get('id');
    this.route.paramMap.pipe(switchMap((params: ParamMap) => this.gameService.getGameById(+params.get('id'))))
      .subscribe(
        game => {
          this.dto = game;
          this.gameService.getCurrentParticipation(this.gameId).subscribe(participation => {
            this.currentParticipation = participation;
          });
          if (!this.dto.active) {
            this.win = true;
            this.getGameResult();
          }
          this.getVoteResult();
          this.gameService.getTarget(this.dto.gameId, 'SLAV').subscribe(res => {
            this.participationIdVotedOn = res;
          });
          this.initializeWebSocketConnection();
        }
      );
    this.getGameStats();
  }

  ngOnInit() {
    this.scrollToBottom();
  }

  ngOnDestroy() {
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
  }

  getGameResult() {
    this.gameService.getGameStats(this.gameId).subscribe(res => {
      this.gameStats = res;
    });
  }

  getGameStats() {
    this.gameService.getGameStats(this.gameId).subscribe(gameStats => {
      this.liveStats = gameStats;
    });
  }

  getRolesByParticipation() {
    this.gameService.getRolesByParticipation(this.dto.gameId).subscribe(roles => {
      this.roles = roles;
    });
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stomp = StompJS.Stomp;
    this.stompClient = this.stomp.over(ws);
    this.stompClient.debug = () => {
    };
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/game/' + that.gameId, (message) => {
        if (message.body) {
          const incoming: Action = JSON.parse(message.body);
          if (incoming.endPhase === true) {
            that.changePhase(incoming);
          } else {

          }
        }
      });
      that.stompClient.subscribe('/game/votes/' + that.gameId, (message) => {
        that.getVoteResult();
      });
    });
  }

  voteMethod($event) {
    this.gameService.vote(this.dto.gameId, $event.participationId).subscribe(res => {
      this.gameService.getTarget(this.dto.gameId, 'SLAV').subscribe(id => {
        this.participationIdVotedOn = id;
      });
    });
  }

  changePhase(action: Action) {
    if (action.win) {
      this.win = action.win;
      this.getGameResult();
    } else {
      this.gameService.getGameById(this.dto.gameId).subscribe(
        game => {
          this.dto = game;
          this.participationIdVotedOn = -1;
        }
      );
    }
    this.getGameStats();
  }

  getVoteResult() {
    if (this.gameId !== undefined) {
      this.gameService.getVoteResult(this.gameId).subscribe((result) => {
        this.voteResults = result;
      });
    }
  }

  doSpecialMove(participationId: number) {
    this.gameService.doSpecialMove(this.gameId, this.selectedRoleAction, participationId).subscribe();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.killBox.nativeElement.scrollTop = this.killBox.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  openTargetModal(role) {
    this.selectedRoleAction = role;
    this.gameService.getTarget(this.gameId, this.selectedRoleAction).subscribe(target => {
      this.target = target;
    });
  }

  getLivingParticipants(): Participation[] {
    if (this.dto !== null && this.dto !== undefined) {
      return this.dto.participations.filter(function (p) {
        return p.alive;
      });
    }

  }

}
