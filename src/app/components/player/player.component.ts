import {Component, EventEmitter, Input, OnChanges, Output, SimpleChange, SimpleChanges} from '@angular/core';
import {Participation} from '../../model/game/participation';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnChanges {

  @Input() player: Participation;
  @Input() votedOn: false;
  @Output() vote: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const player: SimpleChange = changes.player;
    if (player !== undefined) {
      this.player = player.currentValue;
    }
  }

}
