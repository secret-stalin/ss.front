import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbiesPublicComponent } from './lobbies-public.component';
import {AppModule} from '../../app.module';

describe('LobbiesPublicComponent', () => {
  let component: LobbiesPublicComponent;
  let fixture: ComponentFixture<LobbiesPublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbiesPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
