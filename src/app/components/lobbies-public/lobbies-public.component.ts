import {Component, OnInit} from '@angular/core';
import {LobbyService} from '../../services/lobby/lobby.service';
import {Lobby} from '../../model/lobby';
import {Router} from '@angular/router';

@Component({
  selector: 'app-public-lobbies',
  templateUrl: './lobbies-public.component.html',
  styleUrls: ['./lobbies-public.component.scss']
})
export class LobbiesPublicComponent implements OnInit {
  lobbies: Lobby[];

  constructor(private service: LobbyService, private router: Router) {
    this.lobbies = [];
    this.getPublicLobbies();
  }

  ngOnInit() {
  }

  getPublicLobbies() {
    this.service.getPublicLobbys().subscribe(lobbies => {
      lobbies.forEach(l => {
        const lobby = new Lobby();
        lobby.lobbyId = l.lobbyId;
        lobby.users = l.users;
        lobby.gameSettings = l.gameSettings;
        this.lobbies.push(lobby);
      });
    });
  }

  joinLobby(lobbyId: number) {
    this.service.joinLobby(lobbyId).subscribe(value => {
      this.router.navigateByUrl('/lobby/' + lobbyId);
    }, error => {
      alert('The lobby you tried to join is full');
      this.router.navigateByUrl('/join' + lobbyId);
      }
    );
  }

}
