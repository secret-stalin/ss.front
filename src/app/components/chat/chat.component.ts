import {Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked} from '@angular/core';
import * as StompJS from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import {APILOC, TOKEN_NAME} from '../../services/auth.constant';
import {Message} from '../../model/chat/message';
import {ActivatedRoute} from '@angular/router';
import {ChatService} from '../../services/chat/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy, AfterViewChecked {
  messageToSend: Message = new Message();
  private serverUrl = APILOC + '/socket?access_token=' + localStorage.getItem(TOKEN_NAME);
  private stompClient;
  private stomp;
  private chatId;
  messages: Message[];
  private connected = false;

  @ViewChild('scrollMessageBox') private messageBox: ElementRef;

  constructor(private route: ActivatedRoute, private chatService: ChatService) {
    this.chatId = +this.route.snapshot.paramMap.get('id');
    chatService.getExistingMessages(this.chatId).subscribe(res => this.messages = res);
    chatService.getExistingMessages(this.chatId).subscribe((val) => {
      this.messages = val;
      this.initializeWebSocketConnection();
      this.connected = true;
    }, error => {
      this.messages = [];
      const message: Message = new Message();
      message.text = error.error;
      this.messages.push(message);
    });
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stomp = StompJS.Stomp;
    this.stompClient = this.stomp.over(ws);
    this.stompClient.debug = () => {};
    const that = this;
    this.stompClient.connect({}, function (frame) {
      let url = that.stompClient.ws._transport.url;
      url = url.split('\/');
      const sessionId = url[5];
      that.stompClient.subscribe('/chat/' + that.chatId + '-user' + sessionId, (message) => {
        if (message.body) {
          const incoming: Message = JSON.parse(message.body);
          that.messages.push(incoming);
        }
      });
    });
  }

  sendMessage() {
    if (this.connected) {
      this.stompClient.send('/app/send/message/' + this.chatId, {}, JSON.stringify(this.messageToSend));
    }
    $('#input').val('');
    this.messageToSend.text = '';
  }

  ngOnInit() {
    this.scrollToBottom();
  }

  ngOnDestroy() {
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight;
    } catch (err) {}
  }

}
