import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {NotificationMessage} from '../../model/notification/notificationMessage';
import {APILOC, TOKEN_NAME} from '../../services/auth.constant';
import {UserService} from '../../services/user/user.service';
import {NotificationService} from '../../services/notification/notification.service';
import {Router} from '@angular/router';
import {map, startWith} from 'rxjs/operators';
import * as StompJS from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  selected: string;

  isLoggedIn: boolean;
  notifications: NotificationMessage[];
  private serverUrl = APILOC + '/socket?access_token=' + localStorage.getItem(TOKEN_NAME);
  private stompClient;
  private stomp;
  notificationCount: number;

  constructor(private userService: UserService, private notificationService: NotificationService, private router: Router) {
    this.isLoggedIn = userService.isLoggedIn();
    this.notifications = [];
    this.notificationCount = 0;
    this.userService.getAllUsers().subscribe(users => {
      users.forEach(u => this.options.push(u.username));
    });
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.notificationService.getNotificationCount().subscribe(res => {
      this.notificationCount = res;
    });
    this.userService.getSubject().subscribe(res => {
      if (res && !this.isLoggedIn) {
        this.initializeWebSocketConnection();
      }
      this.isLoggedIn = res;
    });
    if (this.isLoggedIn) {
      // subscribe
      this.initializeWebSocketConnection();
    }
  }

  ngOnInit() {

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  navigateToProfile() {
    this.router.navigate(['/user/profile/' + this.selected]);
    this.selected = '';
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stomp = StompJS.Stomp;
    this.stompClient = this.stomp.over(ws);
    this.stompClient.debug = () => {};
    const that = this;
    this.stompClient.connect({}, function (frame) {
      let url = that.stompClient.ws._transport.url;
      url = url.split('\/');
      const sessionId = url[5];
      that.stompClient.subscribe('/notification/1' + '-user' + sessionId, (message) => {
        if (message.body) {
          that.notificationCount = message.body;
        }
      });
    });
  }

  updateNotificationCount(mes) {
    this.notificationService.getNotificationCount().subscribe(res => {
      this.notificationCount = res;
    });
  }

  ngOnDestroy(): void {
    this.userService.getSubject().unsubscribe();
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
  }

  logout(): void {
    this.userService.logout();
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
    this.router.navigate(['/login']);
  }

  getNotifications() {
    this.notificationService.getNotifications().subscribe(notifications => {
      this.notifications = notifications;
    });
  }
}
