import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {User} from '../../model/user';
import {Userstats} from '../../model/userstats';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  username: string;
  user: User = new User();
  isFriend: boolean;
  hasFriendRequest: boolean;
  hasFriendRequestPending: boolean;
  ownProfile: boolean;
  navigationSubscription;
  userStats: Userstats;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.username = this.route.snapshot.paramMap.get('username');
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.username = this.route.snapshot.paramMap.get('username');
        this.ngOnInit();
      }
    });
  }

  ngOnInit() {
    this.userService.getByUsername(this.username).subscribe(user => {
      this.user = user;
      this.ownProfile = this.userService.getEmail() === user.email;
      this.getFriendData();
      this.getUserStats();
    }, error => {
      this.router.navigate(['/error']);
    });
  }

  ngOnDestroy(): void {
    this.navigationSubscription.unsubscribe();
  }

  getUserStats() {
    this.userService.getUserStats(this.username).subscribe(res => this.userStats = res);
  }

  getFriendData() {
    this.userService.findFriends(this.user.email).subscribe(friends => {
      this.user.friends = friends;
    });
    this.userService.isFriendFrom(this.username).subscribe(res => {
      this.isFriend = res;
    });
    if (!this.isFriend) {
      this.userService.hasFriendRequestFrom(this.username).subscribe(res => {
        this.hasFriendRequest = res;
      });
      this.userService.hasFriendRequestPendingToUser(this.username).subscribe(res => {
        this.hasFriendRequestPending = res;
      });
    }
  }

  private navigateTo(username: string) {
    this.router.navigate(['/user/profile/' + username]);
    this.username = this.route.snapshot.paramMap.get('username');
  }

  sendFriendRequest(username: string) {
    this.userService.sendFriendRequest(username).subscribe(res => {
      this.getFriendData();
    });
  }

  acceptFriendRequest(username: string) {
    this.userService.acceptFriendRequest(username).subscribe(res => {
      this.getFriendData();
    });
  }

  declineFriendRequest(username: string) {
    this.userService.declineFriendRequest(username).subscribe(res => {
      this.getFriendData();
    });
  }

  removeAsFriend(username: string) {
    this.userService.removeAsFriend(username).subscribe(res => {
      this.getFriendData();
    });
  }

}
