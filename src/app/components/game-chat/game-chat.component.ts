import {AfterViewChecked, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../services/chat/chat.service';
import {ActivatedRoute} from '@angular/router';
import {Message} from '../../model/chat/message';
import {APILOC, TOKEN_NAME} from '../../services/auth.constant';
import * as StompJS from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import {GameChat} from '../../model/chat/gameChat';

@Component({
  selector: 'app-ingame-chat',
  templateUrl: './game-chat.component.html',
  styleUrls: ['./game-chat.component.scss']
})
export class GameChatComponent implements OnInit, OnDestroy, AfterViewChecked {
  @Input() private gameId: number;
  chats: GameChat[];
  messageToSend: Message = new Message();
  private serverUrl = APILOC + '/socket?access_token=' + localStorage.getItem(TOKEN_NAME);
  private stompClient;
  private stomp;
  private connected = false;

  @ViewChild('scrollMessageBox') private messageBox: ElementRef;

  constructor(private chatService: ChatService, private route: ActivatedRoute) {

  }

  initializeWebSocketConnection(chatId: number) {
    this.connected = true;
    const ws = new SockJS(this.serverUrl);
    this.stomp = StompJS.Stomp;
    this.stompClient = this.stomp.over(ws);
    this.stompClient.debug = () => {};
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/gamechat/' + chatId, (message) => {
        if (message.body) {
          const incoming: Message = JSON.parse(message.body);
          that.chats.find(c => c.id === chatId).messages.push(incoming);
        }
      });
    });
  }

  sendMessage(chatId: number, message: string) {
    this.messageToSend.text = message;
    if (this.connected) {
      this.stompClient.send('/app/gamechat/send/message/' + chatId, {}, JSON.stringify(this.messageToSend));
    }
    $('#input').val('');
    this.messageToSend.text = '';
  }

  ngOnInit() {
    this.chatService.getGameChats(this.gameId).subscribe(res => {
      this.chats = res;
      for (const chat of this.chats) {
        this.initializeWebSocketConnection(chat.id);
      }
    });
    this.scrollToBottom();
  }

  ngOnDestroy() {
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
    }
  }


  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight;
    } catch (err) {}
  }

}
