import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {User} from '../../model/user';
import {Router} from '@angular/router';
import {NotificationSettings} from '../../model/notificationSettings';
import {NotificationService} from '../../services/notification/notification.service';

@Component({
  selector: 'app-profiel-beheer',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  user: User;
  error = '';
  changedUser: User;
  notificationSettings: NotificationSettings;

  constructor(private userService: UserService, private router: Router, private notificationService: NotificationService) {
    this.changedUser = new User();
    this.user = new User();
    this.getUser();
    this.notificationSettings = new NotificationSettings();
    this.getSettings();
  }

  ngOnInit() {

  }

  getUser() {
    this.userService.getUser().subscribe(res => this.user = res);
  }

  saveUser() {
    if (this.changedUser.oldPassword !== undefined && this.changedUser.oldPassword !== '') {
      this.user.oldPassword = this.changedUser.oldPassword;
      this.user.newPassword = this.changedUser.newPassword;
    } else {
      this.user.oldPassword = '';
      this.user.newPassword = '';
      this.changedUser.oldPassword = '';
      this.changedUser.newPassword = '';
    }
    this.userService.saveUser(this.user).subscribe(res => {
      this.router.navigate(['/user/profile/' + this.user.username]);
    }, error => {
      this.error = error.error;
    });
  }

  onFileChanged(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event1: any) => { // called once readAsDataURL is completed
      this.user.profilePicture = event1.target.result;
    };
  }

  getSettings() {
    this.notificationService.readSettings().subscribe(res => this.notificationSettings = res);
  }

  saveSettings() {
    console.log(this.notificationSettings.ignoreEndPhase);
    this.notificationService.sendSettings(this.notificationSettings);
    console.log(this.notificationSettings);
    this.router.navigate(['/user/profile/' + this.user.username]);
  }
}
