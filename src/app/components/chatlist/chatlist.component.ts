import {Component, OnInit} from '@angular/core';
import {Chat} from '../../model/chat/chat';
import {ChatService} from '../../services/chat/chat.service';

@Component({
  selector: 'app-chatlist',
  templateUrl: './chatlist.component.html',
  styleUrls: ['./chatlist.component.scss']
})
export class ChatlistComponent implements OnInit {

  chatList: Chat[];

  constructor(private chatService: ChatService) {
    chatService.getChatsFromUser().subscribe(res => this.chatList = res);
  }

  ngOnInit() {
  }

}
