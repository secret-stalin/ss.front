import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges} from '@angular/core';
import {Game} from '../../model/game/game';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.scss']
})
export class GameboardComponent implements OnInit, OnChanges {

  @Input() game: Game;
  @Input() voteResults;
  @Input() participationIdVotedOn: number;
  @Output() vote: EventEmitter<any> = new EventEmitter<any>();

  playersTop = [];
  playersLeft = [];
  playersRight = [];
  playersBottom = [];

  maxVotes = 0;
  voteResultsStatic;
  oneTime = 0;

  constructor() {
    this.game = new Game();
  }

  ngOnInit() {
    this.fillPlayers();
    this.calculateVotes();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const game: SimpleChange = changes.game;
    if (game !== undefined) {
      this.game = game.currentValue;
      this.fillPlayers();
      this.participationIdVotedOn = -1;
    }

    const voteResults: SimpleChange = changes.voteResults;
    if (voteResults !== undefined) {
      this.voteResults = voteResults.currentValue;
      if (this.voteResults !== undefined) {
        if (voteResults.currentValue.length === 0) {
          this.voteResultsStatic = this.voteResults;
          this.oneTime = 0;
        } else if (this.oneTime === 0) {
          this.voteResultsStatic = this.voteResults;
          this.oneTime = 1;
        }
        this.calculateVotes();
      }
    }
  }

  voteMethod($event) {
    // this.participationIdVotedOn = $event.participationId;
    this.vote.emit({participationId: $event.participationId});
  }

  private fillPlayers() {
    this.playersTop = [];
    this.playersLeft = [];
    this.playersRight = [];
    this.playersBottom = [];
    let i = 0;
    for (const player of this.game.participations) {
      if (i === 0) {
        this.playersTop.push(player);
      } else if (i === 1) {
        this.playersBottom.push(player);
      } else if (i === 2) {
        this.playersRight.push(player);
      } else {
        this.playersLeft.push(player);
        i = -1;
      }
      i++;
    }
  }

  private calculateVotes() {
    this.maxVotes = 0;
    if (this.voteResults !== undefined) {
      for (const c of this.voteResults) {
        this.maxVotes += c.voteCount;
      }
    }
  }
}
